from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, validators
from wtforms.validators import Required, ValidationError

## login and registration


class LoginForm(FlaskForm):
    username = TextField('Username', id='username_login')
    password = PasswordField('Password', id='pwd_login')


class CreateAccountForm(FlaskForm):
    username = TextField('Username', id='username_create')
    email = TextField("Email",[validators.Required(), validators.Email("Please enter your email address.")])
    password = PasswordField('Password', id='pwd_create')
